package main

import (
	"net/http"
	log "github.com/Sirupsen/logrus"
	"os"
	"bitbucket.org/mxplusb/server/internal"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)
}

func main() {
	router := internals.NewRouter()
	log.Fatal(http.ListenAndServe(":8080", router))
}
