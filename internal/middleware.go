package internals

import (
	"net/http"
	log "github.com/Sirupsen/logrus"
	"time"
	"context"
	"github.com/google/uuid"
	"path/filepath"
	"runtime"
)

type key int

const requestIDKey key = 0

func middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		ctx := newContextWithRequestId(r.Context(), r)

		log.WithFields(log.Fields{
			"path":      r.URL.Path,
			"method":    r.Method,
			"loadTime":  time.Since(start).String(),
			"requestID": requestIdFromContext(ctx)}).Info()

		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func newContextWithRequestId(ctx context.Context, r *http.Request) context.Context {
	reqID := r.Header.Get("X-Request-ID")
	if reqID == "" {
		reqID = newRequestId()
	}
	return context.WithValue(ctx, requestIDKey, reqID)
}

func newRequestId() string {
	return uuid.New().String()
}

func requestIdFromContext(ctx context.Context) string {
	return ctx.Value(requestIDKey).(string)
}

func Locate(fields log.Fields) log.Fields {
	_, path, line, ok := runtime.Caller(1)
	if ok {
		_, file := filepath.Split(path)
		fields["file"] = file
		fields["line"] = line
	}
	return fields
}