package internals

import (
	"net/http"
	"io/ioutil"
	log "github.com/Sirupsen/logrus"
	"html/template"
	"strings"
	"context"
	"path/filepath"
	"os"
)

type Page struct {
	Title string
	Body  []byte
}

func loadHtml(title string) (*Page, error) {
	filename := title + ".html"
	body, err := ioutil.ReadFile("./content/" + filename)
	if err != nil {
		return nil, err
	}
	if title == "index" {
		return &Page{Title: "Home", Body: body}, nil
	} else {
		return &Page{Title: title, Body: body}, nil
	}
}

func loadTemplates(ctx context.Context) (*template.Template, error) {
	allFiles := make([]string, 1)

	currPath, err := os.Getwd()
	log.WithFields(Locate(log.Fields{
		"requestID": requestIdFromContext(ctx),
	})).Debug(currPath)
	if err != nil {
		return nil, err
	}

	files, err := ioutil.ReadDir(currPath+"\\templates")
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".tmpl") {
			allFiles = append(allFiles, filepath.Join("templates", filename))
		}
	}
	log.WithFields(Locate(log.Fields{
		"requestID": requestIdFromContext(ctx),
	})).Debugf("found templates: %v", allFiles)


	templates, err := template.ParseFiles(allFiles...)
	if err != nil {
		log.Debug(err)
		return nil, err
	}
	return templates, nil
}

func Home(w http.ResponseWriter, r *http.Request) {
	p := &Page{Title: "Home"}
	templates, err := loadTemplates(r.Context())
	if err != nil {
		log.WithFields(Locate(log.Fields{
			"pageName": p.Title,
			"requestID": requestIdFromContext(r.Context()),
		})).Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	templates.Execute(w, &Page{Title: "Home"})
}
